package signal.flow.graph;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Stack;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Gui extends JFrame {

	private static final long serialVersionUID = 1L;
	Data data;
	JFrame frame = new JFrame();
	TextField fromText = new TextField();
	TextField toText = new TextField();
	JButton enter = new JButton("ENTER");
	JButton undo = new JButton("undo");
	JButton redo = new JButton("redo");
	JButton calculate = new JButton("Calculate");
	JPanel panel = new JPanel();
	JLabel fromLabel = new JLabel("FROM");
	JLabel toLabel = new JLabel("TO");
	JLabel gainLabel = new JLabel("GAIN");
	TextField gainText = new TextField();
	private JPanel canvas = new MyCanvas();
	private Integer[][] graph;
	protected Stack<Node> Undostack = new Stack<Node>();
	protected Stack<Node> Currentstack = new Stack<Node>();

	public Gui() {

		frame.setSize(700, 500);
		panel.setLayout(null);
		fromLabel.setBounds(115, 7, 40, 25);

		panel.add(fromLabel);
		fromText.setBounds(115, 41, 56, 25);
		panel.add(fromText);
		toLabel.setBounds(356, 12, 33, 15);
		panel.add(toLabel);
		toText.setBounds(356, 41, 56, 25);
		panel.add(toText);
		gainLabel.setBounds(581, 12, 33, 15);
		panel.add(gainLabel);
		gainText.setBounds(574, 41, 56, 25);
		panel.add(gainText);
		enter.setBounds(50, 88, 100, 25);
		panel.add(enter);
		calculate.setBounds(200, 88, 100, 25);
		panel.add(calculate);
		undo.setBounds(350, 88, 100, 25);
		panel.add(undo);
		redo.setBounds(550, 88, 100, 25);
		panel.add(redo);
		
		enter.addActionListener(new EnterButton());
		calculate.addActionListener(new CalculateButton());
        undo.addActionListener(new UndoButton());
        redo.addActionListener(new RedoButton());
        
		canvas.setBackground(Color.WHITE);
		canvas.setBounds(0, 120, 700, 350);
		frame.getContentPane().add(canvas, BorderLayout.CENTER);
		frame.getContentPane().add(panel);
		frame.setVisible(true);
		// to draw the nodes
		canvas.repaint();

	}

	private class UndoButton implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
               if(!Currentstack.isEmpty()){
            	   Undostack.push(Currentstack.pop());
            	   Node removed = Undostack.peek();
            	   graph[removed.getFrom()][removed.getTo()] = null ;
            	   canvas.repaint();
               }
		}
	}
	
	private class RedoButton implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
               if(!Undostack.isEmpty()){
            	   Currentstack.push(Undostack.pop());
            	   Node readded = Currentstack.peek();
            	   graph[readded.getFrom()][readded.getTo()] = readded.getWeight() ;
            	   canvas.repaint();
               }
		}
	}
	

	private class EnterButton implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			data = Data.dataInstance();
			graph = data.getGraph();
			int row = Integer.parseInt(fromText.getText()) - 1;
			int col = Integer.parseInt(toText.getText()) - 1;
			int x = Integer.parseInt(gainText.getText());
			if (col == 0 || row == data.getNumberNodes() - 1) {
				JOptionPane.showMessageDialog(frame, "Invalid edge!");
			} else {

				graph[row][col] = x;
				// draw and refresh
				canvas.repaint();
				Currentstack.push(new Node(row , col , x) ) ;
			}

		}
	}

	private class CalculateButton implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// frame.setVisible(false);
			MansonProcess manson = new MansonProcess();
			manson.doManssonProcess();
			DataPrinting printing = new DataPrinting();
			// draw and refresh

		}
	}

}
