package signal.flow.graph;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

public class MansonProcess {

	private Data data;
	private Integer[][] graph;
	private ArrayList<ArrayList<Integer>> forwardPath, loops, nonSortedLoops, nonTouhchingLoops;
	private Map<Integer, ArrayList<ArrayList<Integer>>> combinations;
	//	private Map<Integer, ArrayList<Integer>> gainsOfNonTouching;
	private Stack<Integer> dfsStack = new Stack<Integer>();
	private boolean[] flag;
	private boolean[] visited;
	private ArrayList<Integer>loopsGain;
	private int delta; private float transferFunc;
	private ArrayList<Integer> deltaForEachFwPath;
	public MansonProcess() {

		data = Data.dataInstance();
		//data.setNumberNodes(7);
		/*
		 * references to the graph, FwPaths and loops in Data class
		 */

		graph = data.getGraph();
		loopsGain = new ArrayList<Integer>();
		forwardPath = data.getForwardPath();
		nonSortedLoops = data.getLoop();
		loops = new ArrayList<ArrayList<Integer>>();
		combinations = data.getCombinations();
		nonTouhchingLoops = data.getNonTouhchingLoops();
		visited = new boolean[data.getNumberNodes()];
		deltaForEachFwPath = data.getDeltaForEachFwPath();
		//		gainsOfNonTouching = new HashMap<Integer, ArrayList<Integer>>();

	}
	/*
	 * The function mainly calls all the other functions that, are needed to
	 * find data for Manson process, and proceed the SFG.
	 */

	public void doManssonProcess() {

		findFwPathsAndLoops(0);
		loopsGain =	findGain(nonSortedLoops, true);

		flag = new boolean[data.getLoop().size()];

		findNonTouchingLoops();
		printNonTouchingLoops();
		findNonTouchingCombinations();
		printNonTouchingLpsComins();
		printLoopsGain();
		calcDelta();
		calcDeltaForEachFP();
		printDeltaM();
		getTransferFunc();
		data.setTransferFunc(transferFunc);
	}

	/*
	 * The function uses dfs algorithm to find fw paths in the graph. As well as
	 * the loops of the entire graph. It fills "forwardPath" and "loop"
	 * arrayLists
	 * 
	 * @param start of the graph to start dfs with.
	 * 
	 */

	private void findFwPathsAndLoops(int start) {

		visited[start] = true;
		dfsStack.push(start);

		// check if we reached the end of the forward path
		if (dfsStack.size() > 1 && start == data.getNumberNodes() - 1) {
			ArrayList<Integer> FP = new ArrayList<Integer>();
			System.out.print("Forward path: ");
			for (int i = 0; i < dfsStack.size(); i++) {
				FP.add(dfsStack.get(i));
				System.out.print((dfsStack.get(i) + 1) + " ");
			}
			System.out.println();
			forwardPath.add(FP);
			return;
		}
		for (int i = 0; i < data.getNumberNodes(); i++) {
			if (graph[start][i] != null && !visited[i]) {
				findFwPathsAndLoops(i);
				dfsStack.pop();
				visited[i] = false;
				// condition for detecting loops
			} else if (graph[start][i] != null && visited[i]) {
				int index = dfsStack.indexOf(i);
				if (index != -1) {
					ArrayList<Integer> loop = new ArrayList<Integer>();
					System.out.print("loop ");
					for (int j = index; j < dfsStack.size(); j++) {
						loop.add(dfsStack.get(j));
						System.out.print((dfsStack.get(j) + 1) + " ");
					}
					ArrayList<Integer> nonSorted = new ArrayList<Integer>(loop);
					Collections.sort(loop);
					if (!checkDublicateLoop(loop)) {
						System.out.println("ADDED!!!");
						loops.add(loop);
						nonSortedLoops.add(nonSorted);
					} else {
						System.out.println("NOT ADDED!!!");
					}
				}
			}
		}
		// visited[dfsStack.pop()] = false;
	}

	private void findNonTouchingLoops() {

		for (int i = 0; i < loops.size() - 1; ++i) { // worst case O(n^2)
			Arrays.fill(flag, false);

			ArrayList<Integer> currentLoop = loops.get(i);
			int currentSize = currentLoop.size();

			// stores the index of of the non-touching loops to the current
			// loop.
			ArrayList<Integer> nonTouchloopToCurrentLoop = new ArrayList<Integer>();

			for (int k = 0; k < currentSize; ++k) { // O(currentLoop size)
				int elem = currentLoop.get(k);
				boolean found = false;

				for (int j = i + 1; j < loops.size(); ++j) { // O(loopsSize*Log(otherLoopSize)

					if (flag[j] == true) {
						continue;
					}
					ArrayList<Integer> otherLoop = loops.get(j);
					int lo = 0, hi = otherLoop.size() - 1;
					found = binarySearch(lo, hi, otherLoop, elem);
					if (found) {
						flag[j] = true;
						continue;
					} else if (k == currentSize - 1 && !flag[j]) {
						nonTouchloopToCurrentLoop.add(j);
					}
				}

			}
			nonTouhchingLoops.add(nonTouchloopToCurrentLoop);

		}

	}

	private boolean binarySearch(int lo, int hi, ArrayList<Integer> array, int key) {

		// base case element not found
		if (lo > hi) {
			return false;
		}

		int mid;
		mid = (lo + hi) / 2;

		if (key > array.get(mid)) {

			lo = mid + 1;
			return binarySearch(lo, hi, array, key);

		} else if (key < array.get(mid)) {

			hi = mid - 1;
			return binarySearch(lo, hi, array, key);

		} else {

			return true;

		}

	}

	/*
	 * Finds the different combinations of 2, 3, ... non-touching loops Fills
	 * the array of combinations.
	 */
	private void findNonTouchingCombinations() {
		int size = nonTouhchingLoops.size();

		for (int i = 0; i < size; ++i) {

			ArrayList<Integer> nonTouchTocurrLp = nonTouhchingLoops.get(i);
			/*
			 * this means that there is no non-touching loops to the current
			 * loop, so.. there is no combinations... then go check next node.
			 * 
			 */

			if (nonTouchTocurrLp.isEmpty()) {
				continue;
			}
			ArrayList<ArrayList<Integer>> allCombinsToCurrentLoop = findAllCombinsOfCurrLp(nonTouchTocurrLp);
			combinations.put(i, allCombinsToCurrentLoop);

		}

	}
	/*
	 * Returns all possible combinations of non-touching loop, of the current
	 * loop..
	 * 
	 */

	private ArrayList<ArrayList<Integer>> findAllCombinsOfCurrLp(ArrayList<Integer> nonTouchTocurrLp) {

		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();

		int size = nonTouchTocurrLp.size();

		if (size == 1) {
			ArrayList<Integer> combins = new ArrayList<Integer>();
			combins.add(nonTouchTocurrLp.get(0));
			result.add(combins);

		} else {
			//	boolean flag = false;
			for (int i = 0; i < size - 1; ++i) {
				int loop = nonTouchTocurrLp.get(i);
				ArrayList<Integer> otherLoop = nonTouhchingLoops.get(loop);
				ArrayList<Integer> combins = new ArrayList<Integer>();
				combins.add(loop);
				int loopIndx = 0;
				for (int j = i + 1; j < size; ++j) {

					loopIndx = nonTouchTocurrLp.get(j);
					boolean found = false;
					if (!otherLoop.isEmpty()) {

						found = binarySearch(0, otherLoop.size() - 1, otherLoop, loopIndx);

					}
					/*
					 * Means that the loopIndx found is also non-touching to the
					 * current loop so, a new combination is found
					 */
					if (found) {

						combins.add(loopIndx);
					}
					//					if (loopIndx == nonTouchTocurrLp.get(size - 1) && !found && j == size - 1) {
					//						flag = true;
					//					}
				}
				//if (flag && i == size - 2) {
				if (i == size - 2) {
					//	flag = false;
					ArrayList<Integer> a = new ArrayList<Integer>();
					a.add(loopIndx);
					result.add(combins);
					result.add(a);
					continue;
				}

				result.add(combins);

			}
		}
		return result;
	}

	private boolean checkDublicateLoop(ArrayList<Integer> list) {
		for (int i = 0; i < loops.size(); i++) {
			if (list.equals(loops.get(i))) {
				return true;
			}
		}

		return false;
	}

	private ArrayList<Integer> findGain(ArrayList<ArrayList<Integer>> targetList, boolean isLoop){
		ArrayList<Integer> gains = new ArrayList<Integer>();
		int loopsSize = targetList.size();

		for(int i = 0; i < loopsSize; ++i){
			ArrayList<Integer> currentLoop = targetList.get(i);
			int curSize = currentLoop.size();
			int gain = 1;
			if(curSize == 1 && isLoop){
				gain = graph[currentLoop.get(0)][currentLoop.get(0)];
				gains.add(gain);
				continue;
			}

			for(int j = 0; j < curSize; ++j){

				if(j == curSize - 1 && isLoop){
					gain *= graph[currentLoop.get(j)][currentLoop.get(0)];
					continue;
				}
				else if(j == curSize - 1 && !isLoop){
					continue;
				}
				int x = currentLoop.get(j); int y = currentLoop.get(j + 1);
				gain *= graph[x][y];
			}
			gains.add(gain);
		}
		return gains;
	}
	private void calcDelta(){

		delta = 1;
		/// subtracts the gain of all the loops
		for(int i = 0; i < loops.size(); ++i){

			delta -= loopsGain.get(i);
		}
		delta += calcCombinsGain(combinations);
        data.setDelta(delta);
		System.out.println("========================\n"+"Delta = "+delta);

	}

	private int calcCombinsGain(Map<Integer, ArrayList<ArrayList<Integer>>> combinations){
		int delta =0;
		Map<Integer, ArrayList<Integer>> gainsOfNonTouching = new HashMap<Integer, ArrayList<Integer>>();
		for(Entry<Integer, ArrayList<ArrayList<Integer>>> entry : combinations.entrySet()){

			ArrayList<ArrayList<Integer>> combinsOfCurrKey = entry.getValue();
			int size1 = combinsOfCurrKey.size();
			Integer key = entry.getKey();
			ArrayList<Integer> gainsCombins = new ArrayList<Integer>();
			for(int i = 0; i < size1; ++i){
				ArrayList<Integer> currNontouchingCombins = combinsOfCurrKey.get(i);
				int size2 = currNontouchingCombins.size();
				int gain = loopsGain.get(key);

				for(int j = 0; j < size2; ++j){
					gain*= loopsGain.get(currNontouchingCombins.get(j));
					if(j % 2 != 0){
						gain *= -1;
					}
					if(j < gainsCombins.size() && gainsCombins.get(j)!=null){
						int old = gainsCombins.get(j);
						gainsCombins.set(j, (old + gain));
					}
					else{

						gainsCombins.add(gain);	
					}
				}

			}
			gainsOfNonTouching.put(key, gainsCombins);

		}

		for(Entry<Integer, ArrayList<Integer>> entry : gainsOfNonTouching.entrySet()){

			ArrayList<Integer> gainsCombins = entry.getValue();
			int size = gainsCombins.size();
			for(int i = 0; i < size; ++i){
				int gain = gainsCombins.get(i);
//				if(i % 2 != 0){
//
//					gain *= -1;
//				}
				delta += gain;
			}
		}

		return delta;
	}

	// Calculates delta(m) for each forward path
	private void calcDeltaForEachFP(){
		int deltaM = 1;
		int loopsSize = loops.size(), fwSize =  forwardPath.size();

		for(int i = 0; i < fwSize; ++i){
			ArrayList<Integer> currFp = forwardPath.get(i);
			ArrayList<Integer> nonTouchingToCurrPath = new ArrayList<Integer>();

			for(int j = 0; j < loopsSize; ++j){

				if(isNonTouchingToPath(currFp, loops.get(j))){
					nonTouchingToCurrPath.add(j);
					deltaM -= loopsGain.get(j);
				}
			}
			int size = nonTouchingToCurrPath.size();
			if(size > 1){
				Map<Integer, ArrayList<ArrayList<Integer>>> 
				Fpcombins = new HashMap<Integer, ArrayList<ArrayList<Integer>>>();

				ArrayList<ArrayList<Integer>> 
				newComb = new ArrayList<ArrayList<Integer>>();

				for(int j = 0; j < size; ++j){
					Integer loop = nonTouchingToCurrPath.get(j);

					if(combinations.containsKey(loop)){
						for(Entry<Integer, ArrayList<ArrayList<Integer>>> entry 
								: combinations.entrySet()){

							Integer key = entry.getKey();
							if(key.equals(loop)){
								ArrayList<ArrayList<Integer>> combins = entry.getValue();
								newComb = new ArrayList<ArrayList<Integer>>();
								int size1 = combins.size();
								for(int k = 0;  k < size1; ++k){
									ArrayList<Integer> b = combins.get(k);
									ArrayList<Integer> a = new ArrayList<Integer>(nonTouchingToCurrPath);
									a.remove(key);
									ArrayList<Integer> result = findCombinsNonTouchToCurrPath(a, b);
									
									if(!result.isEmpty()){
										newComb.add(result);
									}	

								}
								/*
								 * Checks that there is no duplicates 
								 * in the resulted combinations,
								 * for the current forward path. 
								 */
								if(newComb.size() > 1){

									Set<ArrayList<Integer>> hs = new HashSet<ArrayList<Integer>>();
									hs.addAll(newComb);
									newComb.clear();
									newComb.addAll(hs);
								}
								break;
							}
						}						
					}
					if(!newComb.isEmpty()){
						Fpcombins.put(loop, newComb);
					}
				}
				deltaM += calcCombinsGain(Fpcombins);
			} 

			deltaForEachFwPath.add(deltaM);
			deltaM = 1;
		}

	}

	private ArrayList<Integer> findCombinsNonTouchToCurrPath(ArrayList<Integer> nonTouchTopath, ArrayList<Integer> combins){
		ArrayList<Integer> result = new ArrayList<Integer>();
		int pSize = nonTouchTopath.size(), cSize = combins.size();
		for(int i = 0; i < pSize; ++i){
			Integer loop = nonTouchTopath.get(i);
			for(int j = 0; j < cSize; ++j){
				if(loop.equals(combins.get(j))){
					result.add(loop);
				}
			}
		}

		return result;
	}

	private boolean isNonTouchingToPath(ArrayList<Integer> path, ArrayList<Integer> loop ){
		int pSize = path.size(), lSize = loop.size();
		int lo = 0, hi = pSize - 1;
		for(int i = 0; i < lSize; ++i){
			int node = loop.get(i);
			if(binarySearch(lo, hi, path, node)){
				return false;
			}
		}
		return true;
	}
	/*
	 * Calculates the final result of Manson process.
	 * To get the final value of the transfer function.
	 */
	private void getTransferFunc(){
		ArrayList<Integer> fpGains = new ArrayList<Integer>();
		int size = forwardPath.size();
		/*Get the gain of each FP 'M'*/
		fpGains = findGain(forwardPath, false);

		int nominator = 0;

		for(int i = 0; i < size; ++i){

			int term = fpGains.get(i) * deltaForEachFwPath.get(i);
			nominator += term;
		}

		transferFunc = (float)((float)nominator / (float)delta);
		System.out.println("=============================\nTransfer Function = " + transferFunc);
	}

	private void printLoopsGain(){
		System.out.println("=============================\nLoops Gain:");
		for(int i = 0; i < loopsGain.size(); i++){

			System.out.println("Loop_"+i+" Gain: "+ loopsGain.get(i));	
		}
	}

	private void printDeltaM(){
		System.out.println("=============================\nDelta For each path:");
		for(int i = 0; i < deltaForEachFwPath.size(); i++){

			System.out.println("Delta Path_"+i+" = "+ deltaForEachFwPath.get(i));	
		}
	}
	private void printNonTouchingLoops() {

		for (int i = 0; i < nonTouhchingLoops.size(); ++i) {
			ArrayList<Integer> a = nonTouhchingLoops.get(i);
			System.out.println("Non-touching to loop_" + i);
			for (int j = 0; j < a.size(); ++j) {

				System.out.print(a.get(j) + ", ");

			}
			System.out.println();
		}
		System.out.println("========================");
	}

	private void printNonTouchingLpsComins() {

		for (Entry<Integer, ArrayList<ArrayList<Integer>>> entry : combinations.entrySet()) {

			ArrayList<ArrayList<Integer>> a = entry.getValue();
			int key = entry.getKey();
			
			//	System.out.println("Loop " + entry.getKey() + " Cominations: ");
			for(int i = 0; i < a.size(); ++i){
				System.out.print(key);
				ArrayList<Integer> b = a.get(i);
				int counter = 0, size = b.size();
				while(counter < size){
					
					System.out.print(", "+ b.get(counter));
					++counter;
				}
				System.out.println();
			}

		}

	}

}