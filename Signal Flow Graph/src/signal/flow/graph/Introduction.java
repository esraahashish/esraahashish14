package signal.flow.graph;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class Introduction extends JFrame  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JFrame frame = new JFrame() ;
	JButton enter = new JButton("OK") ;
	JPanel panel = new JPanel() ;
	Gui gui ;
	Data data ;
	private JTextField numberNodes;
	
	public Introduction() {
		data = Data.dataInstance() ;

		frame.setSize(300, 300 );
        panel.setLayout(null);
        enter.setLocation(118, 141);
        enter.setSize(54, 25);
        panel.add(enter) ;
        enter.addActionListener(new EnterButton() );
		frame.getContentPane().add(panel) ;
		
		numberNodes = new JTextField();
		numberNodes.setBounds(89, 110, 114, 19);
		panel.add(numberNodes);
		numberNodes.setColumns(10);
		
		JLabel lblEnterNumberOf = new JLabel("Enter number of Nodes");
		lblEnterNumberOf.setBounds(62, 47, 173, 67);
		panel.add(lblEnterNumberOf);
	}
	
	private class EnterButton implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			int number = Integer.parseInt( numberNodes.getText() );
			if(number > 20){
				JOptionPane.showMessageDialog(frame, "Maximum number of nodes is 20");
			}
			else{
				data.setNumberNodes( number );
				frame.setVisible(false);
				gui = new Gui();
			}
			
		  } 	
	}
	public void intro(){
		frame.setVisible(true);
	
	}
}
