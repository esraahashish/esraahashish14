package signal.flow.graph;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Arc2D;
import java.awt.geom.Path2D;

import javax.swing.JPanel;

public class MyCanvas extends JPanel {

	private static final long serialVersionUID = 1234567L;

	Data data = Data.dataInstance();
	int numberNodes = data.getNumberNodes();
	int radius = 20;
	int height = 100;
	int nodeSpace = (700 - (numberNodes * radius)) / (numberNodes + 1);

	public MyCanvas() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Graphics getGraphics() {
		Graphics2D g = (Graphics2D) super.getGraphics();
		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(new BasicStroke(8.0f));
		return g2;
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Integer[][] graph = data.getGraph();
		int space = nodeSpace;
		for (int i = 0; i < numberNodes; i++) {
			g.setColor(Color.pink);
			g.fillOval(space, height, radius, radius);
			g.setColor(Color.BLACK);
			g.drawString(String.valueOf(i + 1), space + radius / 2 - 6, height + radius - 6);
			space += (nodeSpace + radius);
		}
		g.setColor(Color.blue);
		for (int i = 0; i < numberNodes; i++) {
			for (int j = 0; j < numberNodes; j++) {

				if (graph[i][j] != null) {
					// forward path between successive nodes
					if (j - i == 1) {
						g.setColor(Color.blue);

						int x = (i + 1) * 20 + (nodeSpace * (i + 1));
						// arrow head
						Path2D arrow = new Path2D.Double();
						arrow.moveTo(x + nodeSpace - 5, height + 10 - 4);
						arrow.lineTo(x + nodeSpace - 5, height + 10 + 4);
						arrow.lineTo(x + nodeSpace + 5, height + 10);
						((Graphics2D) g).fill(arrow);
						// line
						g.drawLine(x, height + 10, x + nodeSpace, height + 10);
						g.drawString(String.valueOf(graph[i][j]), (x + nodeSpace / 2), height + 20);
						// feedback path
					} else if (i - j == 1) {
						g.setColor(Color.red);
						int x = (j + 1) * 20 + (nodeSpace * (j + 1));

						// arrow head
						Path2D arrow = new Path2D.Double();
						arrow.moveTo((x + nodeSpace / 2), height + 30 - 5);
						arrow.lineTo((x + nodeSpace / 2), height + 30 + 5);
						arrow.lineTo((x + nodeSpace / 2) - 5, height + 30);
						((Graphics2D) g).fill(arrow);
						g.drawString(String.valueOf(graph[i][j]), (x + nodeSpace / 2), height + 40);
						// arc
						Arc2D.Float arc = new Arc2D.Float(Arc2D.OPEN);
						arc.setFrame(x, height - 20, nodeSpace, height - 50);
						arc.setAngleStart(0);
						arc.setAngleExtent(-180);
						((Graphics2D) g).draw(arc);
						// forward path between Non-successive nodes
					} else if (j - i > 1) {
						g.setColor(Color.black);
						int x = (i + 1) * 20 + (nodeSpace * (i + 1));

						// arrow head
						Path2D arrow = new Path2D.Double();
						arrow.moveTo((x + (nodeSpace * (j - i) + (j - i - 1) * 20) / 2), (height - 50) - 5);
						arrow.lineTo((x + (nodeSpace * (j - i) + (j - i - 1) * 20) / 2), (height - 50) + 5);
						arrow.lineTo((x + (nodeSpace * (j - i) + (j - i - 1) * 20) / 2) + 5, (height - 50));
						((Graphics2D) g).fill(arrow);
						g.drawString(String.valueOf(graph[i][j]), x + (nodeSpace * (j - i) + (j - i - 1) * 20) / 2,
								(height - 50) + 16);

						// arc
						Arc2D.Float arc = new Arc2D.Float(Arc2D.OPEN);
						g.drawArc(x, 50, nodeSpace * (j - i) + (j - i - 1) * 20, (height - 50) * 2, 0, 180);

					} else if (i - j > 1) {
						g.setColor(Color.GREEN);
						int x = (j + 1) * 20 + (nodeSpace * (j + 1));

						// arrow head
						Path2D arrow = new Path2D.Double();
						arrow.moveTo((x + (nodeSpace * (i - j) + (i - j - 1) * 20) / 2), (height*2 - 50 +40) - 5);
						arrow.lineTo((x + (nodeSpace * (i - j) + (i - j - 1) * 20) / 2), (height*2 - 50 +40) + 5);
						arrow.lineTo((x + (nodeSpace * (i - j) + (i - j - 1) * 20) / 2) - 5, (height*2 - 50+ 40));
						((Graphics2D) g).fill(arrow);
						g.drawString(String.valueOf(graph[i][j]), x + (nodeSpace * (i - j) + (i - j - 1) * 20) / 2,
								(height*2 - 50+ 40) + 16);
						// arc
						Arc2D.Float arc = new Arc2D.Float(Arc2D.OPEN);
						g.drawArc(x, 50, nodeSpace * (i - j) + (i - j - 1) * 20, (height - 50 +20) * 2, 0, -180);
					} else if (i == j) {
						g.setColor(Color.magenta);
						int x = (j + 1) * 20 + (nodeSpace * (j + 1));
						Path2D arrow = new Path2D.Double();
						arrow.moveTo(x + 10, height + 8 - 5);
						arrow.lineTo(x + 10, height + 8 + 5);
						arrow.lineTo(x + 5, height + 8);
						((Graphics2D) g).fill(arrow);
						g.drawString(String.valueOf(graph[i][j]), x + 10, height + nodeSpace/2);

						g.drawOval(x - 5, height + 8, nodeSpace/2, nodeSpace/2);

					}
					/// rest of conditions
				}

			}
		}
	}
}
