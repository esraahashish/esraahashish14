package signal.flow.graph;
import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class DataPrinting extends JFrame {
	private Data data = Data.dataInstance();
	private JFrame frame = new JFrame();
	private JPanel panel = new JPanel();
	private JPanel dataPanel = new JPanel();
	private JLabel label = new JLabel();
	private JMenuBar menuBar = new JMenuBar();
	private JMenu list = new JMenu();
	private JMenuItem ForwardPath = new JMenuItem("ForwardPaths");
	private JMenuItem indiviualLoop = new JMenuItem("indiviualLoop");
	private JMenuItem nonTouchingLoop = new JMenuItem("nonTouchingLoop");
	private JMenuItem delta = new JMenuItem("delta");
	private JMenuItem overAllTF = new JMenuItem("overAllTF");
	private ArrayList<Integer> deltaForEachFwPath = data.getDeltaForEachFwPath();
	private Map<Integer, ArrayList<ArrayList<Integer>>> combinations = data.getCombinations();

	public DataPrinting() {
		frame.setSize(800, 800);
		panel.setLayout(null);
		dataPanel.setLayout(null);
		panel.setBackground(Color.pink);
		dataPanel.setBounds(0, 50, 800, 800);
		dataPanel.setBackground(Color.white);
		menuBar.setBounds(300, 0, 50, 50);
		label.setBounds(300, -60, 300, 150);

		list.add(ForwardPath);
		list.add(indiviualLoop);
		list.add(nonTouchingLoop);
		list.add(delta);
		list.add(overAllTF);
		list.setText("Select");
		list.setVisible(true);
		menuBar.add(list);

		panel.add(menuBar);

		frame.getContentPane().add(dataPanel);
		frame.getContentPane().add(panel);

		frame.setVisible(true);

		ForwardPath.addActionListener(new ForwardPath());
		indiviualLoop.addActionListener(new IndiviualLoop());
		delta.addActionListener(new Delta());
		overAllTF.addActionListener(new OverAllTF());
		nonTouchingLoop.addActionListener(new LoopsCombinations());

	}

	class LoopsCombinations implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			int index = 0 ;
			dataPanel.removeAll();
			label.setText("Non Touching Loops Combinations");
			dataPanel.add(label);
			ArrayList<ArrayList<Integer>> loop = data.getLoop();
			for (Entry<Integer, ArrayList<ArrayList<Integer>>> entry : combinations.entrySet()) {

				ArrayList<ArrayList<Integer>> a = entry.getValue();
				int key = entry.getKey();
				String combination = "Non-Touching to loop ( ";
				for (int i = 0; i < loop.get(key).size(); i++) {
					combination += (loop.get(key).get(i) + 1) + "  ";
				}
				combination += (loop.get(key).get(0) + 1) + " ) : ";

				for (int i = 0; i < a.size(); ++i) {
					ArrayList<Integer> b = a.get(i);
					int counter = 0, size = b.size();
					combination+="{ ";
					while (counter < size) {
						if (counter != 0) {
							combination += ", ";
						}
							combination += "( ";
							for (int j = 0; j < loop.get(b.get(counter)).size(); j++) {
								combination += (loop.get(b.get(counter)).get(j) + 1) + "  ";
							}
							combination += (loop.get(b.get(counter)).get(0) + 1) + " ) ";
						
						++counter;
					}
					combination+=" }  ";
					
				}
				JLabel label_1 = new JLabel(combination);
				label_1.setBounds(50, 50 * ( index++ + 1), 800, 100);
				dataPanel.add(label_1);
				dataPanel.repaint();

			}

		}
	}

	class Delta implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			dataPanel.removeAll();
			label.setText("Delta");
			dataPanel.add(label);
			for (int i = -1; i < deltaForEachFwPath.size(); i++) {
				JLabel label_1 = new JLabel();

				if (i == -1) {
					label_1 = new JLabel("delta : " + data.getDelta());
				} else {
					label_1 = new JLabel("delta for forwardPath_" + (i + 1) + ": " + deltaForEachFwPath.get(i));
				}
				label_1.setBounds(50, 50 * (i + 1), 800, 100);
				dataPanel.add(label_1);
			}
			dataPanel.repaint();
		}

	}

	class OverAllTF implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			dataPanel.removeAll();
			label.setText("Over All Transfer Function");
			dataPanel.add(label);
			Float gain = data.getTransferFunc();
			JLabel label_1 = new JLabel(gain.toString());
			label_1.setBounds(50, 50, 800, 100);
			dataPanel.add(label_1);
			dataPanel.repaint();
		}

	}

	class ForwardPath implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			dataPanel.removeAll();
			label.setText("ForwordPaths");
			dataPanel.add(label);
			draw_forwardPath_loop(data.getForwardPath(), false);
			dataPanel.repaint();
		}

	}

	class IndiviualLoop implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			dataPanel.removeAll();
			label.setText("indiviualLoops");
			dataPanel.add(label);
			draw_forwardPath_loop(data.getLoop(), true);
			dataPanel.repaint();
		}
	}

	private void draw_forwardPath_loop(ArrayList<ArrayList<Integer>> arrayList, boolean loop) {
		JLabel label = new JLabel();
		for (int i = 0; i < arrayList.size(); i++) {
			ArrayList<Integer> list = arrayList.get(i);
			String x = "";
			for (int j = 0; j < list.size(); j++) {
				x += (list.get(j) + 1);
				if (j != list.size() - 1 || loop) {
					x += " ---> ";
				}
				if (loop && j == list.size() - 1) {
					x += (list.get(0) + 1);
				}
			}
			label = new JLabel();
			label.setText(x);
			label.setBounds(50, 50 * (i + 1), 800, 100);
			dataPanel.add(label);
		}
	}

}
