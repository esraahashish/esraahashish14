package signal.flow.graph;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class Data {

	private int numberNodes ;
	private Integer[][] graph ;

	private ArrayList<ArrayList<Integer>> forwardPath = new ArrayList<ArrayList<Integer>>();
	private ArrayList<ArrayList<Integer>> loop = new ArrayList<ArrayList<Integer>>(); 
	private ArrayList<ArrayList<Integer>> nonTouhchingLoops = new ArrayList<ArrayList<Integer>>();
	private Map<Integer, ArrayList<ArrayList<Integer>> > combinations = new LinkedHashMap<Integer, ArrayList<ArrayList<Integer>> >() ;
	private float transferFunc; 
	private ArrayList<Integer> deltaForEachFwPath = new ArrayList<Integer>();
	private int delta;

	public ArrayList<Integer> getDeltaForEachFwPath() {
		return deltaForEachFwPath;
	}

	public float getTransferFunc() {
		return transferFunc;
	}

	public void setTransferFunc(float transferFunc) {
		this.transferFunc = transferFunc;
	}

	public Map<Integer, ArrayList<ArrayList<Integer>> > getCombinations() {
		return combinations;
	}

	public ArrayList<ArrayList<Integer>> getNonTouhchingLoops() {
		return nonTouhchingLoops;
	}

	private static Data firstInstance = new Data() ;

	public static Data dataInstance(){
		return firstInstance;
	}

	private  Data() {
	}

	public void setSize(){
		graph = new Integer[numberNodes][numberNodes] ;
		// rest of data 
	}

	public Integer[][] getGraph() {
		return graph;
	}

	public int getNumberNodes() {
		return numberNodes;
	}

	public void setNumberNodes(int numberNodes) {
		this.numberNodes = numberNodes;
		setSize();
	}

	public ArrayList<ArrayList<Integer>> getForwardPath() {
		return forwardPath;
	}

	public ArrayList<ArrayList<Integer>> getLoop() {

		return loop ;
	}

	public void setDelta(int delta) {
		this.delta = delta;
		
	}
	public int getDelta() {
		return this.delta;
		
	}

}
