#include "Minimization.h"
#include <node.h>
#include <Automata.h>
#include <bits/stdc++.h>


Minimization::Minimization()
{
    log.open ("final_mini.txt");
    aut.open ("final_minimized_auto.txt");
    aut2.open ("final_minimized_auto2.txt");
    aut3.open ("testauAuto");
}
Automata Minimization::get_minimized_dfa()
{
    return minimized_dfa;
}
Minimization::~Minimization() {}
void Minimization::print_automata_itr (Automata automata)
{
    map<int,node> nodes = automata.get_nodes() ;
    vector <string> sv = automata.get_accepted_patterns() ;
    for (std::map<int,node>::iterator it=nodes.begin(); it!=nodes.end(); ++it)
    {
        vector <pair<node,string> > next = (it->second).get_list () ;
        for (int j = 0 ; j < next.size() ; j++)
        {
            aut << it->first  <<" ";
            aut << next[j].second  <<" ";
            aut << next[j].first.get_id() <<endl;
        }
        if ( it->second.get_accepted())
        {
            string s = (it->second).get_accepted_pattern() ;
            aut  << it->second.get_id()<<" accepted node!!! ::"<<s << endl;
        }
    }
}
void Minimization::print_automata_rec (node n, Automata dfa)
{
    vis2[n.id] = true;
    for(int i = 0 ; i < n.next.size(); i++)
    {
        node next = minimized_dfa.nodes.find (n.next[i].first.id)->second;
        string w = n.next[i].second;


        aut2<<n.id<< "," <<next.id <<" -- "<< w ;
        if (next.accepted && next.accepted_pattern.length() != 0){
            aut2 << " --- " << next.accepted_pattern << endl;
        }else {
            aut2 << endl;
        }
        if (!vis2[next.id])
        {
            print_automata_rec(dfa.nodes.find(next.id)->second,dfa);
        }
    }
}
void Minimization::construct_automata()
{
    for (int i=0; i<all_states.size(); i++)
    {
        node rep_node = (minimized_dfa.nodes.find(i))->second;
        node n = initialized_dfa.nodes.find(all_states[i][0].id)->second;
        aut3 << "rp: " << rep_node.id << endl;
        vector < pair <node,string> > next = n.next;
        for (int j=0; j<next.size(); j++)
        {
            node to = next[j].first;
            node next_node = initialized_dfa.nodes.find(to.id)->second;
            int mapped_grp = next_node.mapped_group;
            // node temp = minimized_dfa.nodes.find(mapped_grp)-> second;
            aut3 << "mg: " << mapped_grp << endl;
            rep_node.next.push_back(make_pair(minimized_dfa.nodes.find (mapped_grp)->second , next[j].second));

            aut3 << "from next of rep: " << rep_node.next[rep_node.next.size()-1].first.id << " , "  << rep_node.next[rep_node.next.size()-1].first.accepted_pattern << endl;
            //minimized_dfa.find(next[j].id)
            aut3 << "min node: " << minimized_dfa.nodes.find (mapped_grp)->second.id << endl;
            aut3 << "acc: " << minimized_dfa.nodes.find (mapped_grp)->second.accepted_pattern << endl;
        }
        minimized_dfa.nodes[i] = rep_node;
    }
    aut2 << "rec printing " << endl;
    aut2 << "size " << minimized_dfa.nodes.size()<< endl;
    print_automata_rec(minimized_dfa.nodes.find(minimized_dfa.get_start().id)->second, minimized_dfa);
    aut << "it printing" << endl;
    //aut << "size " << initialized_dfa.nodes.size()<< endl;
    //print_automata_itr(initialized_dfa);
    print_automata_itr(minimized_dfa);





}

void Minimization::init (Automata dfa, vector<string> inputs_list, map<string,int> accep_patterns)
{
    initialized_dfa = dfa;
    inputs = inputs_list;
    accepted_patterns = accep_patterns;
}
vector<vector<node> > Minimization::partition_groups (vector<vector<node> > new_groups, string input)
{
    vector <vector <node> > new_grps;
    map <int,vector<node> > mapped_groups;
    int contained_grp =-1;
    bool next_found = false;

    for (int m = 0; m < new_groups.size(); m++)
    {
        mapped_groups.clear();
        for (int k=0; k<new_groups[m].size(); k++)
        {
            node s = initialized_dfa.nodes.find (new_groups[m][k].id)->second;
            node next_node;
            vector <pair<node, string> > next = s.next;
            contained_grp=-1;
            next_found = false;
            if (next.size () !=0)
            {
                for (int q=0; q<next.size(); q++)
                {
                    if (input.compare(next[q].second) == 0)
                    {

                        next_node  = initialized_dfa.nodes.find (next[q].first.id)->second;
                        next_found = true;



                        break;
                    }
                }
                if (next_found) {
                    for (int f=0; f<all_states.size()&&contained_grp==-1; f++)
                    {
                        for (int r=0; r<all_states[f].size()&&contained_grp==-1; r++)
                        {
                            if (all_states[f][r].id==next_node.id)
                            {
                                contained_grp = f;

                            }
                        }
                    }

                }
            }
            mapped_groups[contained_grp].push_back(s);
        }
        for (map<int, vector<node> >::iterator it = mapped_groups.begin(); it!=  mapped_groups.end(); it++)
        {
            new_grps.push_back ((it)->second);
        }
    }
    return new_grps;
}
void Minimization::minimize_states()
{
    initialize_states ();
    initialize_groups();
    vector<vector<node> >  init;
    vector <vector<node> > new_states;
    vector <vector<node> > new_groups;

    while (true)
    {
        new_states.clear();
        for (int i=0; i < all_states.size(); i++)
        {
            init.clear();
            init.push_back (all_states[i]);
            new_groups.clear();


            for (int j=0; j<inputs.size(); j++)
            {
                if (j==0)
                {
                    new_groups = partition_groups(init, inputs[j]);
                }
                else
                {
                    new_groups = partition_groups(new_groups, inputs[j]);
                }
            }
            for (int k=0; k<new_groups.size(); k++)
            {
                new_states.push_back(new_groups[k]);
            }
        }
        if (new_states.size() == all_states.size())
        {
            break;
        }
        else
        {
            all_states = new_states;
        }
    }
    log << "final result" << endl;
    for (int t=0; t<all_states.size(); t++)
    {
        log << "g " << t << " : ";
        for (int y=0; y<all_states[t].size(); y++)
        {
            log << all_states[t][y].id << " ";
        }
        log << "\n" ;
    }

    aut2 << "before constructing : " << endl;

    for (int i=0; i<all_states.size(); i++)
    {
        node n;
        n.id = i;
        //aut2 << "g " << i << ": " ;
        for (int k=0; k<all_states[i].size(); k++)
        {


            //aut2 << all_states[i][k].id << ", " ;
            node old_node = initialized_dfa.nodes.find(all_states[i][k].id)->second;
            old_node.mapped_group =  i;


            initialized_dfa.nodes[old_node.id] = old_node;
            all_states[i][k] = old_node;
            if (all_states[i][k].accepted)
            {
                n.accepted_pattern = all_states[i][k].accepted_pattern;
                n.accepted = true;

                //aut2 << " -- ac -- " << all_states[i][k].accepted_pattern << ", ";
            }
            if (old_node.id == initialized_dfa.get_start().id)
            {
                minimized_dfa.set_start(n);
            }
        }
        //aut2 << endl;
        minimized_dfa.nodes[i] = n;

        aut2 << "    ********       " << endl;
        aut2 << "rep : "<< n.id << " , " << n.accepted_pattern << endl;
        aut2 << "    ********       " << endl;
    }
    construct_automata();
}
void Minimization::initialize_states()
{
    vector<node>group;
    all_states.push_back(group);
    int tokens_num = accepted_patterns.size();
    for(int i = 0 ; i < tokens_num ; i++)
    {
        vector<node>group;
        all_states.push_back(group);
    }

    for (std::map<string,int>::iterator it = accepted_patterns.begin(); it != accepted_patterns.end(); it++){
        aut2 << it->first << endl ;
    }aut2 << endl;
}

void Minimization::initialize_groups()
{
    aut2 << " ----------  grps ---------------- " << endl;
    for (std::map<int,node>::iterator it =initialized_dfa.nodes.begin(); it!=initialized_dfa.nodes.end(); ++it)
    {
        node n_state = it->second;
        aut2 << "n: "<< n_state.id << " " ;
        if(n_state.accepted)
        {
            int mapped_group =  accepted_patterns.find(n_state.accepted_pattern)->second;
            n_state.mapped_group = mapped_group;
            all_states[mapped_group].push_back(n_state);

            aut2 << "-- acc : " << n_state.accepted_pattern << " mg: " << mapped_group << endl;
        }
        else
        {
            n_state.mapped_group = 0;
            all_states[0].push_back(n_state);
            aut2 << " mg: " << 0 << endl;;
        }
    }
}
/*
}
// recursive method
void Minimization::initialize_groups(node n_state)
{
    cout <<"n: " << n_state.id << endl;
    vis1[n_state.id] = true;
    if(n_state.accepted)
    {
        int mapped_group =  accepted_patterns.find(n_state.accepted_pattern)->second;
        n_state.mapped_group = mapped_group;
        all_states[mapped_group].push_back(n_state);
    }
    else
    {
        n_state.mapped_group = 0;
        all_states[0].push_back(n_state);
    }
    vector < pair <node,string> > next = n_state.next;
    for(int i = 0 ; i < next.size() ; i++)
    {
        node q = initialized_dfa.nodes.find(next[i].first.id)->second;
        if (!vis1[q.id])
        {
            initialize_groups(q);
        }
    }
}
*/



