package eg.edu.alexu.csd.filestructure.hash;


import java.nio.MappedByteBuffer;

import java.util.ArrayList;


import javax.management.RuntimeErrorException;


public class QuadraticProbing<K, V> implements IHash<K, V>, IHashQuadraticProbing {


	private int capacity = 1200;

	private final int deleted = -1;

	ArrayList<Pair> hashTable = new ArrayList<Pair>(1200);

	private int size = 0;

	private int countCollisions = 0;


	public QuadraticProbing() {

		inititialize();

	}


	private void rehash() {

		capacity = capacity * 2;

		Pair pair = null;

		int mappedKey = 0;

		int countProbes = 0;

		ArrayList<Pair> tempHashTable = (ArrayList<Pair>) this.hashTable.clone();

		hashTable = new ArrayList<Pair>(capacity);

		inititialize();


		for (int i = 0; i < (capacity / 2); i++) {

		 if(tempHashTable.get(i)!=null && !tempHashTable.get(i).getKey().equals(deleted)){	

			pair = new Pair(tempHashTable.get(i).getKey(), tempHashTable.get(i).getValue());

			mappedKey = pair.getKey().hashCode() % capacity;

			countProbes = 1;

            boolean flag = false;

			while (countProbes <= capacity && hashTable.get(mappedKey) != null) {

				flag = true;

				mappedKey = (pair.getKey().hashCode() % capacity + countProbes * countProbes) % capacity;

				countProbes++;

				countCollisions++;

			}

            if(flag){

            	countCollisions++;

            }

			hashTable.set(mappedKey, pair);

		 }


		}


	}


	private void inititialize() {

		for (int i = 0; i < capacity; i++) {

			hashTable.add(null);

		}

	}


	@Override

	public void put(Object key, Object value) {

		// full HashTable

		if (size == capacity) {

			countCollisions++;

			rehash();

		}


		

		while(true){

				Integer mappedKey = key.hashCode() % capacity;

				int countProbes = 1;

		        boolean flag = false;

			while (countProbes <= capacity && hashTable.get(mappedKey) != null && !hashTable.get(mappedKey).getKey().equals(deleted)) {

                flag = true;

				countCollisions++;

				mappedKey = (key.hashCode() % capacity + countProbes * countProbes) % capacity;

				countProbes++;


			}

			if(hashTable.get(mappedKey) != null && !hashTable.get(mappedKey).getKey().equals(deleted)){

				countCollisions++;

				rehash();

			}else{

				if(flag){

					countCollisions++;

				}

				hashTable.set(mappedKey, new Pair(key, value));

				size++;

				break;

			}


	}


	}


	@Override

	public String get(Object key) {


		int mappedKey = key.hashCode() % capacity;

		if (mappedKey < 0) {

			throw new RuntimeErrorException(null);

		}

		int probingCounter = 0;

		int initialKey = mappedKey;

		int countProbes = 1;


		while ((probingCounter != capacity) && (hashTable.get(mappedKey) != null)) {


			if (hashTable.get(mappedKey).getKey().equals(key)) {

				return hashTable.get(mappedKey).getValue();

			}


			probingCounter++;

			mappedKey = (initialKey + countProbes * countProbes) % capacity;

			countProbes++;

		}

		return null;

	}


	@Override

	public void delete(Object key) {

		int mappedKey = key.hashCode() % capacity;

		if (mappedKey < 0) {

			throw new RuntimeErrorException(null);

		}

		int probingCounter = 0;

		int initialKey = mappedKey;

		int countProbes = 1;


		while ((probingCounter != capacity) && (hashTable.get(mappedKey) != null)) {


			if (hashTable.get(mappedKey).getKey().equals(key)) {

				hashTable.get(mappedKey).setKey(deleted);

				size--;

				return;

			}


			mappedKey = (initialKey + countProbes * countProbes) % capacity;

			countProbes++;

			probingCounter++;

		}

	}


	@Override

	public boolean contains(Object key) {


		return get(key) != null;

	}


	@Override

	public boolean isEmpty() {


		return size == 0 ? true : false;

	}


	@Override

	public int size() {


		return this.size;

	}


	@Override

	public int capacity() {


		return this.capacity;

	}


	@Override

	public int collisions() {


		return this.countCollisions;

	}


	@Override

	public Iterable keys() {

		ArrayList<Object> keys = new ArrayList<Object>(size);

		if (size != 0) {

			for (Pair pair : hashTable) {

				if (pair != null && pair.getKey() != deleted) {

					keys.add(pair.getKey());

				}

			}


			return keys;

		}

		return null;

	}


}
