package eg.edu.alexu.csd.filestructure.hash;

import java.util.LinkedList;

public class HashChaining<K,V> implements IHash<V, K> , IHashChaining {
	private final int capacity = 1200;
	private LinkedList<Pair>[] hashTable = new LinkedList[capacity];
	private Pair pair;
	private int size = 0;
	private int countCollisions = 0;
	public HashChaining() {
		for(int i = 0 ; i < capacity ; i++){
			hashTable[i] = new LinkedList<Pair>();
		}
	}

	@Override
	public void put(Object key, Object value) {

		pair = new Pair(key, value);
		int mappedKey = key.hashCode()%capacity;
		countCollisions+=hashTable[mappedKey].size();
		size++;
		hashTable[mappedKey].add(pair);
	}

	@Override
	public String get(Object key) {

		int mappedKey = key.hashCode()%capacity;

		if (hashTable[mappedKey] != null) {
			for (Pair itr : hashTable[mappedKey]) {

				if (itr.getKey().equals(key))
					return itr.getValue();
			}
		}
		return null;
	}

	@Override
	public void delete(Object key) {

		int mappedKey = key.hashCode()%capacity;
		if (hashTable[mappedKey] != null) {
			for (Pair itr : hashTable[mappedKey]) {

				if (itr.getKey().equals(key)){
					hashTable[mappedKey].remove(itr);
					size--;
					return;
				}
			}
		}

	}

	@Override
	public boolean contains(Object key) {
		return get(key) != null ;
	}

	@Override
	public boolean isEmpty() {

		return this.size == 0 ? true : false;
	}

	@Override
	public int size() {

		return this.size;
	}

	@Override
	public int capacity() {
		// TODO Auto-generated method stub
		return this.capacity;
	}

	@Override
	public int collisions() {
		// TODO Auto-generated method stub
		return this.countCollisions;
	}

	@Override
	public Iterable keys() {

		LinkedList<Object> iterable = new LinkedList<Object>();
		if (!isEmpty()) {

			for (LinkedList<Pair> slotList : hashTable) {

				for (Pair pair : slotList) {

					iterable.add(pair.getKey());
				}
			}

			return iterable;

		} else {

			return null;
		}
	}

}
