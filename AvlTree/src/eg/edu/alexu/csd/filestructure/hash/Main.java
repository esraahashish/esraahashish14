package eg.edu.alexu.csd.filestructure.hash;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinearProbing linear = new LinearProbing<>();
		QuadraticProbing quadratic = new QuadraticProbing() ;
		HashChaining chaining = new HashChaining<>();
		DoubleHashing doubleHash = new DoubleHashing<>();
		for(int i=0; i<10000; i++){
 			int key = (i+100000) * 12345;
 			linear.put(key, String.valueOf(i));
 			doubleHash.put(key, String.valueOf(i));
 			quadratic.put(key, String.valueOf(i));
 			chaining.put(key, String.valueOf(i));
 		}
		System.out.println("Chaining collision:"+chaining.collisions());
		System.out.println("double collision:"+doubleHash.collisions());
		System.out.println("Linear collision:"+linear.collisions());
		System.out.println("Quadratic collision:"+quadratic	.collisions());

		System.out.println("Chaining capacity:"+chaining.capacity());
		System.out.println("double capacity:"+doubleHash.capacity());
		System.out.println("Linear capacity:"+linear.capacity());
		System.out.println("Quadratic capacity:"+quadratic.capacity());
	}

}
