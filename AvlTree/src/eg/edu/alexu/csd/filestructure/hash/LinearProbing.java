package eg.edu.alexu.csd.filestructure.hash;

import java.lang.reflect.Array;
import java.util.ArrayList;

import javax.management.RuntimeErrorException;

public class LinearProbing<K,V> implements IHash<K,V> ,IHashLinearProbing{

	private  int capacity = 1200;
	private final int deleted = -1;
	ArrayList<Pair> hashTable = new ArrayList<Pair>(capacity);
	private int size = 0;
	private int countCollisions = 0;
	public LinearProbing() {
		inititialize();
	}
	
	@Override
	public void put(Object key, Object value) {
		
		int counter = 1;
		//full HashTable
		if(size == capacity){
			countCollisions++;
			countCollisions+=capacity;
			reshash();
		}
		
		int mappedKey = key.hashCode()%capacity;
		boolean  flag = false;
		while(hashTable.get(mappedKey)!=null && !hashTable.get(mappedKey).getKey().equals(deleted) ){
			countCollisions++;
			flag = true ;
			mappedKey = ( key.hashCode()%capacity + counter ) % capacity ;
			counter++;	
		}
		if(flag){
			countCollisions++;
		}
		hashTable.set(mappedKey , new Pair(key , value ) );
		size++;	
	}

	private void reshash() {
		
		capacity = capacity*2;
		
		ArrayList<Pair> tempHashTable = (ArrayList<Pair>) this.hashTable.clone();
		hashTable = new ArrayList<Pair>(capacity);
		inititialize();
		
		for(int i = 0 ; i < capacity/2 ; i++){
			Pair pair = new Pair(tempHashTable.get(i).getKey() , tempHashTable.get(i).getValue());
			int mappedKey = pair.getKey().hashCode() % capacity;
			boolean flag = false;
			int counter = 1 ;
			while(hashTable.get(mappedKey) != null){
				flag = true;
				mappedKey = ( pair.getKey().hashCode() % capacity + counter ) % capacity ;
				countCollisions++;
				counter++;
			}
			if(flag){
				countCollisions++;
			}
			hashTable.set(mappedKey , pair );
		}
	}
	
	private void inititialize(){
		for(int i = 0 ; i < capacity ; i++){
	    	hashTable.add(null);
	    }
	}

	@Override
	public String get(Object key) {
		int mappedKey = key.hashCode() % capacity;
		int probingCounter = 0 ;
			
		while( (probingCounter != capacity) && (hashTable.get(mappedKey) != null) ){
			
			if(hashTable.get(mappedKey).getKey().equals(key)){
				return hashTable.get(mappedKey).getValue();
			}
			
			probingCounter++;
			mappedKey = ( mappedKey + 1 ) % capacity ;
		}
		return null;
	}

	@Override
	public void delete(Object key) {
        int mappedKey = key.hashCode()%capacity;
        int probingCounter = 0 ;
        
		while( (probingCounter != capacity) && (hashTable.get(mappedKey) != null) ){
			
			if(hashTable.get(mappedKey).getKey().equals(key)){
				 hashTable.get(mappedKey).setKey(deleted);
				 size--;
				 return ;
			}
			
			mappedKey = ( mappedKey + 1 ) % capacity ;
			probingCounter++;
		}	
	}

	@Override
	public boolean contains(Object key) {
		
		return get(key) != null;
	}

	@Override
	public boolean isEmpty() {
		
		return size == 0 ? true : false ;
	}

	@Override
	public int size() {
		
		return this.size;
	}

	@Override
	public int capacity() {
		
		return this.capacity;
	}

	@Override
	public int collisions() {
	
		return this.countCollisions;
	}

	@Override
	public Iterable keys() {
		ArrayList<Object> keys = new ArrayList<Object>(size) ;
		if(size!=0){
			for(Pair pair : hashTable){
				if(pair!=null&&pair.getKey()!=deleted){
					keys.add(pair.getKey());
				}
			}
			
			return keys;
		}
		return null;
	}

}
//if(hashTable.get(mappedKey).getKey() == key){
//hashTable.get(mappedKey).setValue((String) value);
//return;
//}
