package eg.edu.alexu.csd.filestructure.hash;

public class Pair {
	private Integer hashkey;
	private String value;

	public Pair(Object hashKey, Object value) {
		setKey((Integer) hashKey);
		setValue((String) value);
	}

	public Integer getKey() {
		return hashkey;
	}

	public void setKey(Integer key) {
		this.hashkey = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
