package eg.edu.alexu.csd.filestructure.hash;

import java.util.ArrayList;

import javax.management.RuntimeErrorException;

import eg.edu.alexu.csd.filestructure.hash.Pair;

public class DoubleHashing<K, V> implements IHash<V, K>, IHashDouble {
	private int capacity = 1200;
	private final int deleted = -1;
	private int prime = 1193;
	ArrayList<Pair> hashTable = new ArrayList<Pair>(1200);
	private Pair pair;
	private int size = 0;
	private int countCollisions = 0;

	public DoubleHashing() {
		inititialize();
	}

	private void rehash() {

		capacity = capacity * 2;

		Pair pair = null;

		int mappedKey = 0;

		int countProbes = 0;

		ArrayList<Pair> tempHashTable = (ArrayList<Pair>) this.hashTable.clone();

		hashTable = new ArrayList<Pair>(capacity);

		inititialize();

		for (int i = 0; i < (capacity / 2); i++) {

			if (tempHashTable.get(i) != null && !tempHashTable.get(i).getKey().equals(deleted)) {

				pair = new Pair(tempHashTable.get(i).getKey(), tempHashTable.get(i).getValue());

				mappedKey = pair.getKey().hashCode() % capacity;

				countProbes = 1;

				while (countProbes <= capacity && hashTable.get(mappedKey) != null) {

					mappedKey = (pair.getKey().hashCode() % capacity
							+ countProbes * (prime - ((int) pair.getKey() % prime))) % capacity;

					countProbes++;

					countCollisions++;

				}

				hashTable.set(mappedKey, pair);

			}

		}

	}

	private void inititialize() {

		for (int i = 0; i < capacity; i++) {

			hashTable.add(null);

		}

	}

	@Override

	public void put(Object key, Object value) {

		// full HashTable

		if (size == capacity) {

			countCollisions++;

			rehash();

		}

		while (true) {

			Integer mappedKey = key.hashCode() % capacity;

			int countProbes = 1;

			while (countProbes <= capacity && hashTable.get(mappedKey) != null
					&& !hashTable.get(mappedKey).getKey().equals(deleted)) {

				countCollisions++;

				mappedKey = (key.hashCode() % capacity + countProbes * (prime - (key.hashCode() % prime))) % capacity;

				countProbes++;

			}

			if (hashTable.get(mappedKey) != null && !hashTable.get(mappedKey).getKey().equals(deleted)) {

				countCollisions++;

				rehash();

			} else {


				hashTable.set(mappedKey, new Pair(key, value));

				size++;

				break;

			}

		}

	}

	@Override

	public String get(Object key) {

		int mappedKey = key.hashCode() % capacity;

		if (mappedKey < 0) {

			throw new RuntimeErrorException(null);

		}

		int probingCounter = 0;

		int initialKey = mappedKey;

		int countProbes = 1;

		while ((probingCounter != capacity) && (hashTable.get(mappedKey) != null)) {

			if (hashTable.get(mappedKey).getKey().equals(key)) {

				return hashTable.get(mappedKey).getValue();

			}

			probingCounter++;

			mappedKey = (key.hashCode() % capacity + countProbes * (prime - ((int) key % prime))) % capacity;

			countProbes++;

		}

		return null;

	}

	@Override

	public void delete(Object key) {

		int mappedKey = key.hashCode() % capacity;

		if (mappedKey < 0) {

			throw new RuntimeErrorException(null);

		}

		int probingCounter = 0;

		int initialKey = mappedKey;

		int countProbes = 1;

		while ((probingCounter != capacity) && (hashTable.get(mappedKey) != null)) {

			if (hashTable.get(mappedKey).getKey().equals(key)) {

				hashTable.get(mappedKey).setKey(deleted);

				size--;

				return;

			}

			mappedKey = (key.hashCode() % capacity + countProbes * (prime - ((int) key % prime))) % capacity;

			countProbes++;

			probingCounter++;

		}

	}

	@Override

	public boolean contains(Object key) {

		return get(key) != null;

	}

	@Override

	public boolean isEmpty() {

		return size == 0 ? true : false;

	}

	@Override

	public int size() {

		return this.size;

	}

	@Override

	public int capacity() {

		return this.capacity;

	}

	@Override

	public int collisions() {

		return this.countCollisions;

	}

	@Override

	public Iterable keys() {

		ArrayList<Object> keys = new ArrayList<Object>(size);

		if (size != 0) {

			for (Pair pair : hashTable) {

				if (pair != null && pair.getKey() != deleted) {

					keys.add(pair.getKey());

				}

			}

			return keys;

		}

		return null;

	}

	private void checkPrime(int num) {
		int check = 0;

		for (int i = 2; i < num; i++) {
			if (num % i == 0) {
				check = 0;
				break;
			} else {
				check = 1;
			}
		}
		if (check == 1 || num == 2) {
			prime = num;
		} else {
			if (num > 2)
				checkPrime(num - 1);
		}
	}

}
