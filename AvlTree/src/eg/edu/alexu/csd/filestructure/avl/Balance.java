package eg.edu.alexu.csd.filestructure.avl;

public class Balance<T extends Comparable<T>> {

	public Node<T> balance(Node<T> root) {

		if (root == null) {
			return root;
		}

		// left subtree insertion
		if (height((Node<T>) root.getLeftChild()) - height((Node<T>) root.getRightChild()) > 1) {

			if (height((Node<T>) root.getLeftChild().getLeftChild()) >= height(
					(Node<T>) root.getLeftChild().getRightChild())) {
				root = rotateLeftLeft(root);
			} else {
				root = rotateRightLeft(root);
			}

			// right insertion
		} else if (height((Node<T>) root.getRightChild()) - height((Node<T>) root.getLeftChild()) > 1) {

			if (height((Node<T>) root.getRightChild().getRightChild()) >= height(
					(Node<T>) root.getRightChild().getLeftChild())) {
				root = rotateRighRight(root);
			} else {
				root = rotateLeftRight(root);
			}

		}

		root.setHeight(Math.max(height((Node<T>) root.getLeftChild()), height((Node<T>) root.getRightChild())) + 1);
		return root;
	}

	private Node<T> rotateLeftRight(Node<T> root) {
		root.setRight(rotateLeftLeft((Node<T>) root.getRightChild()));
		return rotateRighRight(root);
	}

	private Node<T> rotateRighRight(Node<T> root) {

		Node<T> temp = (Node<T>) root.getRightChild();
		root.setRight((Node<T>) temp.getLeftChild());
		temp.setLeft(root);
		root.setHeight(Math.max(height((Node<T>) root.getLeftChild()), height((Node<T>) root.getRightChild())) + 1);
		temp.setHeight(Math.max(height((Node<T>) temp.getRightChild()), root.getHeight()) + 1);

		return temp;
	}

	private Node<T> rotateRightLeft(Node<T> root) {

		root.setLeft(rotateRighRight((Node<T>) root.getLeftChild()));
		return rotateLeftLeft(root);
	}

	private Node<T> rotateLeftLeft(Node<T> root) {

		Node<T> temp = (Node<T>) root.getLeftChild();
		root.setLeft((Node<T>) temp.getRightChild());
		temp.setRight(root);
		root.setHeight(Math.max(height((Node<T>) root.getLeftChild()), height((Node<T>) root.getRightChild())) + 1);
		temp.setHeight(Math.max(height((Node<T>) temp.getLeftChild()), root.getHeight()) + 1);

		return temp;
	}

	private int height(Node<T> node) {
		return node == null ? -1 : node.getHeight();
	}
}
