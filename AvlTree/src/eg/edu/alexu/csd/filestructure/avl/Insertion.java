package eg.edu.alexu.csd.filestructure.avl;

public class Insertion<T extends Comparable<T>> {
	Balance<T> balance = new Balance<T>();

	public Node<T> insert(T value, Node<T> root) {

		if (root == null) {
			return new Node<T>(value, null, null);
		}

		int compareResult = value.compareTo(root.getValue());

		// less than
		if (compareResult < 0) {

			root.setLeft(insert(value, (Node<T>) root.getLeftChild()));

		} else if (compareResult > 0) {

			root.setRight(insert(value, (Node<T>) root.getRightChild()));

		} else {
			Node<T> temp = new Node<T>(value, null, (Node<T>) root.getRightChild());
            root.setRight(balance.balance(temp) );
            return balance.balance(root) ;
		}

		return balance.balance(root);
	}

}
