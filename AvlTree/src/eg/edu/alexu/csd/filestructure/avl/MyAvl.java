package eg.edu.alexu.csd.filestructure.avl;

public class MyAvl<T extends Comparable<T>> implements IAVLTree<T> {

	private Node<T> root = null;
	private Insertion<T> insertion = new Insertion<T>();

	@Override
	public void insert(T key) {

		if (key != null) {

			if (root == null) {

				root = new Node<T>(key, null, null);

			}else{ 

				root = insertion.insert(key, root);
			}
		}

	}

	// public Node<T> get () {
	// return root ;
	// }
	 public void print(Node<T> r) {
	 if(r== null){
	 return ;
	 }
	 print((Node<T>) r.getLeftChild());
	 System.out.println(r.getValue());
	 print((Node<T>) r.getRightChild());
	 }

	@Override
	public boolean delete(T key) {
		Deletion<T> deletion = new Deletion<T>();

		// special cases for deleting root or empty avlTree
		if (root == null || key == null || !search(key)) {
			return false;
		} else if (root.getValue().compareTo(key) == 0 && height() == 1) {
			root = null;
			return true;
		}

		Node<T> deleted = deletion.delete(key, root);

		if (deleted != null) {

			root = deleted;
			return true;

		} else {

			return false;

		}
	}

	@Override
	public boolean search(T key) {
		SearchKey<T> search = new SearchKey<T>();

		if (key == null || search.search(key, root) == null) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public int height() {

		if (root == null) {
			return 0;
		}

		return root.getHeight() + 1;
	}

	@Override
	public INode<T> getTree() {
		// TODO Auto-generated method stub
		return this.root;
	}

}
