package eg.edu.alexu.csd.filestructure.avl;

import java.io.File;

public class Main {

	public static void main(String[] args) {
		MyAvl<Integer> avl = new MyAvl();
		avl.insert(3);
		avl.insert(0);
		avl.insert(2);
		avl.insert(1);
		avl.insert(4);
		avl.insert(2);
		
//		avl.insert(2);
//		avl.insert(1);
//		avl.insert(4);
//		avl.insert(3);
//		avl.insert(5);
//		avl.insert(4);
		avl.print((Node<Integer>) avl.getTree());
		//System.out.println(avl.height());

	}

}
