package eg.edu.alexu.csd.filestructure.avl;

public class Deletion<T extends Comparable<T>> {

	private Balance<T> balance = new Balance<T>();
	private boolean flag = false;

	public Node<T> delete(T key, Node<T> root) {

		if (root == null) {
			flag = true;
			return null;

		}

		int compare = key.compareTo(root.getValue());

		if (compare < 0) {

			root.setLeft(delete(key, (Node<T>) root.getLeftChild()));

		} else if (compare > 0) {

			root.setRight(delete(key, (Node<T>) root.getRightChild()));

		} else if (root.getLeftChild() != null && root.getRightChild() != null) {

			root.setValue(getSuccessor(root.getRightChild()).getValue());
			root.setRight(delete(root.getValue(), (Node<T>) root.getRightChild()));

		} else {

			root = root.getRightChild() == null ? (Node<T>) root.getLeftChild() : (Node<T>) root.getRightChild();
		}

		if (!flag) {
			return balance.balance(root);
		} else {
			return null;
		}
	}

	private Node<T> getSuccessor(INode<T> node) {

		if (node.getLeftChild() != null) {
			return getSuccessor(node.getLeftChild());
		}

		return (Node<T>) node;
	}
}
