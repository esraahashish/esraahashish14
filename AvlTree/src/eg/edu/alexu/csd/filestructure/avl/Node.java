package eg.edu.alexu.csd.filestructure.avl;

public class Node<T extends Comparable<T>> implements INode<T> {
	private T value = null;
	private Node<T> left = null;
	private Node<T> right = null;
	private int height = -1;

	public Node(T value, Node<T> left, Node<T> right) {
		this.value = value;
		this.right = right;
		this.left = left;
		height = 0;
	}

	public void setRight(Node<T> right) {
		this.right = right;
	}

	public void setLeft(Node<T> left) {
		this.left = left;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getHeight() {
		return this.height;
	}

	public Node() {
		this(null, null, null);
	}

	@Override
	public INode<T> getLeftChild() {

		return this.left;
	}

	@Override
	public INode<T> getRightChild() {

		return this.right;
	}

	@Override
	public T getValue() {

		return this.value;
	}

	@Override
	public void setValue(T value) {

		this.value = value;
	}

}
