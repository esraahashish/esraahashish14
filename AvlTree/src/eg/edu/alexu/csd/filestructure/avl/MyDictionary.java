package eg.edu.alexu.csd.filestructure.avl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MyDictionary implements IDictionary {

	private IAVLTree avl = new MyAvl();
	private int size = 0;
	@Override
	public void load(File file) {
		BufferedReader br = null;

		try {

			String sCurrentLine;
			br = new BufferedReader(new FileReader(file));

			while ((sCurrentLine = br.readLine()) != null) {

				if (!exists(sCurrentLine)) {

					avl.insert(sCurrentLine);
					size++;

				}
			}

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (br != null) {

					br.close();
				}

			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		// avl.print(avl.get());

	}

	@Override
	public boolean insert(String word) {

		if (exists(word)) {

			return false;

		} else {

			avl.insert(word);
			size++;
			return true;
		}
	}

	@Override
	public boolean exists(String word) {

		return avl.search(word);
	}

	@Override
	public boolean delete(String word) {

		if (avl.delete(word)) {
			size--;
			return true;
		} else {
			return false;
		}

	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return this.size;
	}

	@Override
	public int height() {
		// TODO Auto-generated method stub
		return avl.height();
	}

}
