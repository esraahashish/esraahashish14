package eg.edu.alexu.csd.filestructure.avl;

public class SearchKey<T extends Comparable<T>> {

	public Node<T> search(T value, Node<T> node) {

		if (node == null) {
			return null;
		}

		int compare = value.compareTo(node.getValue());

		if (compare < 0) {
			return search(value, (Node<T>) node.getLeftChild());
		} else if (compare > 0) {
			return search(value, (Node<T>) node.getRightChild());
		} else {
			return node;
		}
	}
}
