package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;
import java.util.Random;

public class Main {

	public static <T extends Comparable<T>> void main(String[] args) {

		ArrayList<Integer> list = new ArrayList<Integer>();
		MySort<T> sort = new MySort<T>();
		
        int size;
		long startTime;
		long stopTime;
		long elapsedTime;

		// generate random elements in arrayList
		Random randomGenerator = new Random();
		size = randomGenerator.nextInt(10000);
		for (int idx = 0; idx <= size; ++idx) {
			list.add(randomGenerator.nextInt(100));
		}

		// test execution time for heap sort
		startTime = System.currentTimeMillis();
		sort.heapSort((ArrayList) list.clone());
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		System.out.println("HeapSort execution time : " + (stopTime - startTime));

		// test execution time for insertion sort
		startTime = System.currentTimeMillis();
		sort.sortSlow((ArrayList) list.clone());
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		System.out.println("Insertion execution time : " + (stopTime - startTime));

		// test execution time for insertion sort
		startTime = System.currentTimeMillis();
		sort.sortFast((ArrayList) list.clone());
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		System.out.println("Quick execution time : " + (stopTime - startTime));

	}

}
