package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;

public class MyHeap<T extends Comparable<T>> implements IHeap<T> {
	private ArrayList<INode<T>> heap = new ArrayList<INode<T>>();
	private int size = 0;
	
	public void setSize(int size){
		this.size = size ;
	}
	
	public ArrayList< INode <T> > getHeap(){
		return this.heap ;
	}
	
	public void sort(ArrayList<T> unordered){
		int initialSize = unordered.size() ;
        build(unordered);
    	
    	for(int i = initialSize - 1 ; i >0 ; i-- ){
    		T temp = heap.get(i).getValue() ;
    		heap.get(i).setValue(heap.get(0).getValue());
    		heap.get(0).setValue(temp);
    		setSize(i);
    		heapify(heap.get(0));
    	}
    	
    	setSize(initialSize);
	}

	@Override
	public INode<T> getRoot() {

		if (size != 0) {
			return heap.get(0);
		} else {
			return null;
		}
	}

	@Override
	public int size() {

		return this.size;
	}

	@Override
	public void heapify(INode<T> node) {
		
		INode<T> left = node.getLeftChild();
		INode<T> right = node.getRightChild();
		INode<T> max = new INodeImpl();

		if ((left != null) && left.getValue().compareTo(node.getValue()) > 0) {
			max = left;
		} else {
			max = node;
		}

		if ((right != null) && right.getValue().compareTo(max.getValue()) > 0) {
			max = right;
		}

		if (max != null && max.getValue().compareTo(node.getValue()) != 0) {
			T temp = max.getValue();
			max.setValue(node.getValue());
			node.setValue( (T) temp);
			heapify(max);
		}
	}

	//Run in O(lg n) time
	@Override
	public T extract() {
		T max ;
		if(size==0){
		  return null;
		}else if(size<=2){
			max = getRoot().getValue() ;
			heap.remove(0);
			size--;
			return (T) max;
	    }else{
		    max = getRoot().getValue() ;
			heap.get(0).setValue(heap.get(size-1).getValue());
			heap.remove(size-1) ;
			size--;
			heapify(heap.get(0));
			return (T) max ; 
		}
	}


	@Override
	public void insert(T element) {
		INode<T> newNode = new INodeImpl(size , element) ;
		heap.add(size, newNode);
		size++;
		INode<T> parent = new INodeImpl() ; 
	    parent = newNode.getParent() ;
		while(parent!=null && element.compareTo(parent.getValue()) > 0 ) {
			newNode.setValue(parent.getValue());
			parent.setValue((T) element); 
			newNode = parent ;
			parent = parent.getParent() ;
		}
	}

	// build in O(n) ;
	@Override
	public void build(java.util.Collection<T> unordered) {
		int j = 0;
		INode<T> node;
		this.size = unordered.size();

		// set heap with nodes ;
		for (T iterable_element : unordered) {
			node = new INodeImpl(j++ , iterable_element);
			heap.add(node);
		}

		for (int i = (size / 2 )-1  ; i >= 0; i--) {
			this.heapify(heap.get(i));
		}
		
	}

	// INode Implementation
	public class INodeImpl implements INode<T> {
		private T value ;
		private int index;

		public INodeImpl() {
		}

		public INodeImpl(int index , T value) {
			this.index = index;
			this.value = value ;
		}

		@Override
		public INode<T> getLeftChild() {
			int idx = ((2 * INodeImpl.this.index )+1);
			if (idx >= size()) {
				return null;
			}
			return (INode<T>) heap.get(idx);
		}

		@Override
		public INode<T> getRightChild() {
			int idx = (2 * INodeImpl.this.index) + 2;
			if (idx >= size()) {
				return null;
			}
			return (INode<T>) heap.get(idx);
		}

		@Override
		public INode<T> getParent() {
			int idx = (INodeImpl.this.index -1 ) / 2;

			if (INodeImpl.this.index == 0) {
				return null;
			}
			return (INode<T>) heap.get(idx);
		}

		@Override
		public T getValue() {
			return (T) this.value ;
		}

		@Override
		public void setValue(T value) {
			this.value = value;
		}

	}

}
