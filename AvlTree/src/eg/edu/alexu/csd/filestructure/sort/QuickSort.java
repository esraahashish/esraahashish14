package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;

public class QuickSort {
public <T> void sort(java.util.ArrayList<T> arr , int low , int high){
	if(low>=high)return ;
		int index = partion(arr , low , high);
		sort(arr , low , index-1);
		sort(arr, index+1 , high);
	
}

private <T> int partion(java.util.ArrayList<T> arr ,int low , int high) {
	// TODO Auto-generated method stub
	int lowIndex = low;
	int highIndex = high+1;
	while(true){
		
		while(((Comparable) arr.get(low)).compareTo(arr.get(++lowIndex)) > 0 ){
			if(lowIndex==high)break;
		}
		while(((Comparable) arr.get(low)).compareTo(arr.get(--highIndex)) < 0){
			if(highIndex==low)break;
		}
		//swap i & j
	    if(lowIndex<highIndex){
	    T temp =  arr.get(lowIndex) ;
		arr.set(lowIndex, arr.get(highIndex));
		arr.set(highIndex, (T) temp);
	    }else{
	    	T temp =  arr.get(low) ;
			arr.set(low, arr.get(highIndex));
			arr.set(highIndex, temp);
			return highIndex;
	    }
	
	}
	
 }
}
