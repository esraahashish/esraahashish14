package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;

public class MySort<T extends Comparable<T>> implements ISort<T>{
	
    @Override//
	public IHeap<T> heapSort(java.util.ArrayList<T> unordered) {
    	 MyHeap<T> heap = new MyHeap<T>() ;	
    	 heap.sort(unordered);
		return heap;
	}

    // insertion sort
	@Override
	public void sortSlow(java.util.ArrayList<T> unordered) {
		int idx1 ;
		int idex2 ;
		T  newValue;

	      for (idx1 = 1; idx1 < unordered.size(); idx1++) {

	            newValue = unordered.get(idx1);

	            idex2 = idx1;

	            while (idex2 > 0 && unordered.get(idex2 - 1).compareTo(newValue) > 0 ) {

	            	unordered.set(idex2,  unordered.get(idex2 - 1));

	                  idex2--;

	            }

	            unordered.set(idex2, newValue);

	      }

	}
	

	@Override
	public void sortFast(java.util.ArrayList<T> unordered) {
		QuickSort quick = new QuickSort();
		quick.sort(unordered, 0, unordered.size()-1);
	}

}
