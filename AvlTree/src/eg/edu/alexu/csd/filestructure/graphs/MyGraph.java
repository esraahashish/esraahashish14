package eg.edu.alexu.csd.filestructure.graphs;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.management.RuntimeErrorException;

public class MyGraph implements IGraph {

	private boolean negativeWeight = false;
	private ArrayList<Integer> node = new ArrayList<Integer>();
	private ArrayList<GraphEdge> graph = new ArrayList<GraphEdge>();
	private ArrayList<ArrayList<Point>> adjacencyList = new ArrayList<ArrayList<Point>>();
	private ArrayList<Integer> DijkstraOrder = new ArrayList<Integer>();
	ArrayList<ArrayList<Integer>> neighbours = new ArrayList<ArrayList<Integer>>();
	private int nodes = -1;
	private int edges = -1;
	private int from;
	private int to;
	private int weight;

	@Override
	public void readGraph(File file) {

		BufferedReader br = null;

		try {

			String sCurrentLine;
			br = new BufferedReader(new FileReader(file));
			sCurrentLine = br.readLine();
			if (sCurrentLine != null) {
				String[] splited1 = sCurrentLine.split("\\s+");

				if (splited1.length != 2) {
					throw new RuntimeErrorException(null);
				} else {

					try {
						nodes = Integer.parseInt(splited1[0]);
						edges = Integer.parseInt(splited1[1]);
						if (nodes <= 0 || edges < 0) {
							throw new RuntimeErrorException(null);
						} else {
							initialize();
						}
					} catch (Exception e) {
						throw new RuntimeErrorException(null);
					}
				}
			} else {
				throw new RuntimeErrorException(null);
			}

			for (int i = 0; i < edges && (sCurrentLine = br.readLine()) != null; i++) {

				String[] splited = sCurrentLine.split("\\s+");
				if (splited.length != 3) {
					throw new RuntimeErrorException(null);
				} else {
					try {
						from = Integer.parseInt(splited[0]);
						to = Integer.parseInt(splited[1]);
						weight = Integer.parseInt(splited[2]);
						if (from < 0 || from >= nodes || to < 0 || to >= nodes) {
							throw new RuntimeErrorException(null);
						}
					} catch (Exception e) {
						throw new RuntimeErrorException(null);
					}
					if (weight < 0) {
						negativeWeight = true;
					}

					ArrayList<Point> adjacent = adjacencyList.get(from);
					adjacent.add(new Point(to, weight));
					graph.add(new GraphEdge(from, to, weight));

					ArrayList<Integer> neighbourFrom = neighbours.get(from);

					if (!neighbourFrom.contains(to)) {
						neighbourFrom.add(to);
					}

					if (!node.contains(from)) {
						node.add(from);
					}

					if (!node.contains(to)) {

						node.add(to);
					}
				}
			}

			if (sCurrentLine == null) {
				throw new RuntimeErrorException(null);
			}

		} catch (IOException e) {
			throw new RuntimeErrorException(null);

		} finally {

			try {

				if (br != null) {
					br.close();
				}

			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	private void initialize() {
		for (int i = 0; i < nodes; i++) {
			graph.add(new GraphEdge());
			adjacencyList.add(new ArrayList<Point>());
			neighbours.add(new ArrayList<Integer>());
		}
	}

	@Override
	public int size() {
		return this.edges;

	}

	@Override
	public ArrayList<Integer> getVertices() {
		return this.node;
	}

	@Override
	public ArrayList<Integer> getNeighbors(int v) {
		return this.neighbours.get(v);
	}

	@Override
	public void runDijkstra(int src, int[] distances) {
		if (!negativeWeight) {
			Dijkstra dijkstra = new Dijkstra();
			DijkstraOrder = dijkstra.dijkstra(src, distances, nodes, adjacencyList);
		}
	}

	@Override
	public ArrayList<Integer> getDijkstraProcessedOrder() {
		return this.DijkstraOrder;

	}

	@Override
	public boolean runBellmanFord(int src, int[] distances) {
		BellmanFord bellman = new BellmanFord();

		return bellman.bellmanFord(src, distances, nodes, graph);

	}

}
