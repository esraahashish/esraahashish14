package eg.edu.alexu.csd.filestructure.graphs;

import java.util.ArrayList;

public class BellmanFord {
    final private int INF = Integer.MAX_VALUE / 2; 
    private boolean finished =  false ;
	public boolean bellmanFord(int src, int[] distances , int nodes ,  ArrayList<GraphEdge> graph ){
	
		initialize(distances , nodes);
		distances[src] = 0 ;
		
		for(int i=0 ; i<nodes-1 && !finished;i++){
		    finished = true ;
			for(int j = 0 ; j < graph.size() ; j++){
		    	if(distances[graph.get(j).getTo()]>graph.get(j).getWeight() +distances[graph.get(j).getFrom()]){
		    		distances[graph.get(j).getTo()] = graph.get(j).getWeight() +distances[graph.get(j).getFrom()];
		    	    finished = false ;
		    	}
		    }
		}
		
		for (int j  = 0; j < graph.size() && !finished; ++j)
            if (distances[graph.get(j).getTo()]>graph.get(j).getWeight() +distances[graph.get(j).getFrom()]){
                 return false;
            }
        return true;  
   }
	
	private  void initialize(int[] distances , int nodes){
		for(int i = 0 ; i < nodes ; i++){
			distances[i] = INF ;
		}
	}
}
