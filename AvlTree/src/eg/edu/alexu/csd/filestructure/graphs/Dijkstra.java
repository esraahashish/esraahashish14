package eg.edu.alexu.csd.filestructure.graphs;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Dijkstra {
	private Comparator<Point> comparator;
	private PriorityQueue<Point> queue;
	private ArrayList<Integer> scanned ;
    private boolean [] visited ;
	final int INF = Integer.MAX_VALUE / 2;
    
	public Dijkstra() {
		scanned = new ArrayList<Integer>();
		comparator = (Comparator<Point>) new WeightComparator();
		queue = new PriorityQueue<Point>( comparator);
	}
	
	public ArrayList<Integer> dijkstra(int src, int[] distances, int nodes , ArrayList<ArrayList<Point>> adjacencyList) {
		initialize(distances, nodes);
		visited = new boolean[nodes];
		distances[src] = 0 ;
		queue.add(new Point(src , distances[src]));
		
		while(!queue.isEmpty()){
			Point scan =  queue.peek();
			queue.remove();
			
			if(!visited[scan.x]){
				
				visited[scan.x] = true ;
				scanned.add(scan.x);
				ArrayList<Point> current = adjacencyList.get(scan.x) ;
				for(int i = 0 ; i < current.size() ; i++){
					
					if(distances[current.get(i).x] > distances[scan.x] + current.get(i).y){
						
						distances[current.get(i).x] = (int) (distances[scan.x] + current.get(i).y) ;
						queue.add(new Point(current.get(i).x , distances[current.get(i).x]));
						
					}
				}
			}
			
		}
		return scanned;
		
	}

	private void initialize(int[] distance ,int  nodes){
		for(int i = 0; i < nodes ;i++){
			distance[i]=INF;
		}
	}
}

class WeightComparator implements Comparator<Point> {

	@Override
	public int compare(Point p1, Point p2) {
		if (p1.getY() < p2.getY()) {
			return -1;
		} else if (p1.getY() > p2.getY()) {
			return 1;
		} else {
			return 0;
		}
	}

}
