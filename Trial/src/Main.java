import java.util.*;
import java.util.concurrent.CountDownLatch;


public class Main {

	
	static class Node {
		Node r ;
		Node l ;
		Integer value ;
	}
	
	public static void main(String[] args) {
		
		Node root = new Node();
		Node n1 =root;
		n1.value = 6 ;
		Node n2 = new Node();
		n2.value = 4 ;
		root.value = 5 ;
		root.l = n1;
		root.r = n2;
		Integer count = 0 ;
		
		System.out.println(root == n1);
	}
	
	 static int countNodes(Node current , int level , Integer count , int max)
	{
		if(current == null)
		{
			return count
					;
		}
		
		if(level > max)
		{
			count += current.value;
		}
		
		return  countNodes(current.l, level+1, count, max) + countNodes(current.r, level+1, count, max);
		
	}


}
